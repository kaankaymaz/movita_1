package controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.MessageFormat;

public class MenuControl extends WebControl {

    public static final String MENU_TOP = "//a[@class=\"menu-item\"][contains(., \"{0}\")] | //a[@class=\"menu-link\"][contains(., \"{0}\")]";
    public static final String MENU_LEFT = "//li[contains(., \"{0}\") and .//ancestor::ul[1][@id=\"main-menu-navigation\"]]";


    public MenuControl(By locator) {
        super(locator);
    }

    public static MenuControl fromLabel(String label) {
        return fromLabel(label, 1);
    }
    public static MenuControl fromLabel(String label, int no) {

        String xpath = "(" +
                MessageFormat.format(MENU_TOP, label) +
                " | " +
                MessageFormat.format(MENU_LEFT, label) +
                ")[" + no +"]";


        By textFieldLocator = By.xpath(xpath);
        MenuControl control = new MenuControl(textFieldLocator);
        control.label = label;
        return control;
    }




    public static MenuControl fromLocator(By locator) {
        MenuControl control = new MenuControl(locator);
        return control;
    }


    public String getText() {
        WebElement rootElement = getRootElement();
        return rootElement.getAttribute("value");
    }


}
