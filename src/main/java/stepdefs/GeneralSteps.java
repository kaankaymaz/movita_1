package stepdefs;

import controls.MenuControl;
import io.cucumber.java.en.When;

public class GeneralSteps {

    @When("^hover over menu (Anasayfa|Raporlar|AracRota|AracVideoFotograf|YapayZeka|GüzergahIslemleri|AlarmIslemleri|RotaIslemleri|PersinelIslemleri|ReklamAnonsVideo|MüsteriIslemleri|YönetimVeAyarlar)$")
    public void hoverOverElementWithAttribute(String menu){
        switch (menu){
            case "Anasayfa" -> MenuControl.fromLabel("Anasayfa ( filo_admin ) ").hover();
            case "Raporlar" -> MenuControl.fromLabel("Raporlar").hover();
            case "AracRota" -> MenuControl.fromLabel("Araç Rota").hover();
            case "AracVideoFotograf" -> MenuControl.fromLabel("Araç Video-Fotoğraf").hover();
            case "YapayZeka" -> MenuControl.fromLabel("Yapay Zeka").hover();
            case "GüzergahIslemleri" -> MenuControl.fromLabel("Güzergah İşlemleri").hover();
            case "AlarmIslemleri" -> MenuControl.fromLabel("Alarm İşlemleri").hover();
            case "RotaIslemleri" -> MenuControl.fromLabel("Rota İşlemleri").hover();
            case "PersinelIslemleri" -> MenuControl.fromLabel("Personel İşlemleri").hover();
            case "ReklamAnonsVideo" -> MenuControl.fromLabel("Reklam-Anons-Video").hover();
            case "MüsteriIslemleri" -> MenuControl.fromLabel("Müşteri İşlemleri").hover();
            case "YönetimVeAyarlar" -> MenuControl.fromLabel("Yönetim ve Ayarlar").hover();
        }
    }
}

