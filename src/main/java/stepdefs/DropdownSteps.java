package stepdefs;

import actions.DropdownActions;
import io.cucumber.java.en.When;

public class DropdownSteps {

    @When("user clicks the {string} dropdown {string}")
    public void clickDropdown(String label , String type) {
        DropdownActions.click(label);
        DropdownActions.click(type);

    }



}
